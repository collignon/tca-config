# Source : https://dirask.com/posts/Bash-interactive-menu-example-arrow-up-and-down-selection-jm3YGD
# GLOBAL VARIABLES
VERSION=1.4
SHARED_FOLDER=/c/nassur/dumps/partage
ANONYMISATION_FOLDER=/c/nassur/config/backup_bdd/anonymise-pseudonymise/scripts_pseudonymise

# FILENAMES
STATIC_FILE=statics.sql
PARAMS_FILE=params.sql
FAITS_ROUTINES=faits_routines.sql
FAITS=faits.sql
FAITS_VIEWS=faits_views.sql
##########################################################################
function usage()
{
    echo "Usage : "
    echo "  --cygwin                            Si le bash est celui Cygwin"
    echo "  -v|--version                        Affiche la version du script"
    echo "  -f=<FILE>|--file=<FILE>             Fichier zip, défaut dans $SHARED_FOLDER"
    echo "  -h|--help                           Documentation"
    echo "  -u=<USER>|--user=<USER>             Mysql user"
    echo "  -p=<PASSWORD>|--password=<PASSWORD> Mysql password"
    echo "  -e=<ENV>|--env=<ENV>                Environment : [REC, PREPROD, PROD]"
    echo "  -c=<CLIENT>|--client=<CLIENT>       Nom du client (préfix des noms bdd)"
    echo "  --default                           Choix des sql par defaut"
    exit 0
}
##########################################################################
function showLogo()
{
	echo "            _                      _       _  _  "
	echo "   __      | |    __ _     __     | |__   | || | "
	echo "  / _|     | |   / _\` |   / _|    | / /    \_, |   "
	echo "  \__|_   _|_|_  \__,_|   \__|_   |_\_\   _|__/  "
	echo "_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_| \"\"\"\"| "
	echo "\"\`-0-0-'\"\`-0-0-'\"\`-0-0-'\"\`-0-0-'\"\`-0-0-'\"\`-0-0-'  v${VERSION}"
	echo ""
}
##########################################################################
rm -f *.copy
for i in "$@"; do
  	case $i in
	  	-v|--version)
            showLogo
            echo "Clacky : v${VERSION}"
            exit 0
            shift
            ;;
		-f=*|--file=*)
			FILE="${i#*=}"
			shift
			;;
		--cygwin)
			ANONYMISATION_FOLDER=/cygdrive/c/nassur/config/backup_bdd/anonymise-pseudonymise/scripts_pseudonymise
			SHARED_FOLDER=/cygdrive/c/nassur/dumps/partage
			shift
            ;;
        -u=*|--user=*)
        	USER_INPUT="${i#*=}"
			shift
			;;
		-p=*|--password=*)
        	PASSWORD_INPUT="${i#*=}"
			shift
			;;
		-e=*|--env=*)
			ENV_INPUT="${i#*=}"
			shift
			;;
		-c=*|client=*)
			CLIENT_INPUT="${i#*=}"
			shift
			;;
		--default)
			DEFAULT_FILE=true
			;;
		-h|--help)
            usage
            ;;
		*)
			echo "Argument inconnu"
            usage
			;;
  esac
done
if [[ ! -z $FILE ]] && [[ ! -f $FILE ]]
then
	echo "File not found ${FILE}"
	exit 1
fi
##########################################################################
##### OVERRIDE DU FICHIER ENV PAR LES ARGS EN PARAMETRE
if [[ ! -z $USER_INPUT ]]
then
	USER=$USER_INPUT
fi
if [[ ! -z $PASSWORD_INPUT ]]
then
	PASSWORD=$PASSWORD_INPUT
fi
if [[ ! -z $ENV_INPUT ]]
then
	ENV=$ENV_INPUT
fi
if [[ ! -z $CLIENT_INPUT ]]
then
	CLIENT=$CLIENT_INPUT
fi
if [[ -z $USER ]] || [[ -z $PASSWORD ]] || ( [[ -z $ENV ]] && [[ -z $FILE ]] ) || [[ -z $CLIENT ]]
then
	echo "Certains arguments sont manquants : "
	echo "MYSQL_USER : "${USER}
	echo "MYSQL_PASSWORD : "${PASSWORD}
	echo "ENV : "${ENV} " Remplir si FILE est vide"
	echo "FILE : "${FILE}
	echo "CLIENT : "${CLIENT}
	echo " "
	usage
    exit 1
fi
##########################################################################
function copyLastDump()
{
	if [[ ! -z $FILE ]]
	then
		COPY=$(basename "$FILE")
		EXTENTION="${COPY##*.}"
		FILENAME="${COPY%.*}"
		cp $FILE ${FILENAME}.copy
		return
	fi
	echo "Copie du dernier dump de ${ENV}"
	rm -f ${ENV}_DUMP_NASSUR_last_dump.zip
	yes | cp -rf ${SHARED_FOLDER}/${ENV}_DUMP_NASSUR_last_dump.zip .
}
##########################################################################
function removeFileInArray()
{
	local function_arguments=($@)
	list=(${function_arguments[@]:1})
	delete=$1
	for i in ${!list[@]};do
	    if [[ "${list[$i]}" =~ $delete ]]; then
	        unset list[$i]
	    fi
	done
	echo ${list[@]}
}
##########################################################################
function print_menu()  # selected_item, ...menu_items
{
    local function_arguments=($@)

    local selected_item="$1"
    local menu_items=(${function_arguments[@]:1})
    local menu_size="${#menu_items[@]}"

    for (( i = 0; i < $menu_size; ++i ))
    do
        if [ "$i" = "$selected_item" ]
        then
            echo "[x]   ${menu_items[i]}"
        else
            echo "[ ]   ${menu_items[i]}"
        fi
    done
}
##########################################################################
function run_menu()  # selected_item, ...menu_items
{
    local function_arguments=($@)

    local selected_item="$2"
    local menu_items=(${function_arguments[@]:2})
    local menu_size="${#menu_items[@]}"
    local menu_limit=$((menu_size - 1))

    clear
    showLogo
    echo "Selectionner le fichier des" $1
    print_menu "$selected_item" "${menu_items[@]}"
    while read -rsn1 input
    do
        case "$input"
        in
            $'\x1B')  # ESC ASCII code (https://dirask.com/posts/ASCII-Table-pJ3Y0j)
                read -rsn1 -t 0.1 input
                if [ "$input" = "[" ]  # occurs before arrow code
                then
                    read -rsn1 -t 0.1 input
                    case "$input"
                    in
                        A)  # Up Arrow
                            if [ "$selected_item" -ge 1 ]
                            then
                                selected_item=$((selected_item - 1))
                                clear
                                showLogo
                                echo "Selectionner le fichier des" $1
                                print_menu "$selected_item" "${menu_items[@]}"
                            fi
                            ;;
                        B)  # Down Arrow
                            if [ "$selected_item" -lt "$menu_limit" ]
                            then
                                selected_item=$((selected_item + 1))
                                clear
                                showLogo
                                echo "Selectionner le fichier des" $1
                                print_menu "$selected_item" "${menu_items[@]}"
                            fi
                            ;;
                    esac
                fi
                read -rsn5 -t 0.1  # flushing stdin
                ;;
            "")  # Enter key
                return $selected_item
                ;;
        esac
    done
}
##########################################################################
function getSqlFilename()
{
	IFS='|' read -ra linesSplited <<< "$1"
	for i in "${linesSplited[@]}"; do
  		if [[ $i == *".sql"* ]]; then
  			echo $i
  			return
  		fi
	done
}
##########################################################################
function getDefaultSelection()  # selected_item, ...menu_items
{
  	local function_arguments=($@)

    local filter="$1"
    local menu_items=(${function_arguments[@]:1})
    local menu_size="${#menu_items[@]}"
	local j=0
	for i in ${!menu_items[@]};do

	    if [[ "${menu_items[$i]}" =~ $filter ]]; then
	        echo $j
			return
	    fi
		j=$(($j+1))
	done
	echo 0
}
##########################################################################
if [[ ! -z $DEFAULT_FILE ]] && [[ $DEFAULT_FILE ]]
then
	NOTHING=""
else
	if [[ ! -z $FILE ]]
	then
		7z l -ba $FILE > sqlFiles.txt
	else
		7z l -ba $${ENV}_DUMP_NASSUR_last_dump.zip > sqlFiles.txt
	fi
	sed -i 's/\s\+/|/g' sqlFiles.txt
	IFS=$'\n' read -d '' -r -a lines < sqlFiles.txt
	rm sqlFiles.txt


	selected_item=$(getDefaultSelection "static" ${lines[@]})
	run_menu "statics" $selected_item ${lines[@]}
	choosen=${lines[$?]}
	STATIC_FILE=$(getSqlFilename $choosen)
	lines=$(removeFileInArray $STATIC_FILE ${lines[@]})
	IFS=' ' read -d '' -r -a lines <<< $lines


	selected_item=$(getDefaultSelection "params" ${lines[@]} )
	run_menu "params" $selected_item ${lines[@]}
	choosen=${lines[$?]}
	PARAMS_FILE=$(getSqlFilename $choosen)
	lines=$(removeFileInArray $PARAMS_FILE ${lines[@]})
	IFS=' ' read -d '' -r -a lines <<< $lines


	selected_item=$(getDefaultSelection "faits_routines" ${lines[@]} )
	run_menu "faits_routines" $selected_item ${lines[@]}
	choosen=${lines[$?]}
	FAITS_ROUTINES=$(getSqlFilename $choosen)
	lines=$(removeFileInArray $FAITS_ROUTINES ${lines[@]})
	IFS=' ' read -d '' -r -a lines <<< $lines


	selected_item=$(getDefaultSelection "faits_views" ${lines[@]} )
	run_menu "faits_views" $selected_item ${lines[@]}
	choosen=${lines[$?]}
	FAITS_VIEWS=$(getSqlFilename $choosen)
	lines=$(removeFileInArray $FAITS_VIEWS ${lines[@]})
	IFS=' ' read -d '' -r -a lines <<< $lines


	selected_item=$(getDefaultSelection "faits" ${lines[@]} )
	run_menu "faits" $selected_item ${lines[@]}
	choosen=${lines[$?]}
	FAITS=$(getSqlFilename $choosen)
fi
##########################################################################
function unzipLastDump()
{
	echo "Décompression du dump"
	if [[ ! -z $FILE ]]
	then
		7z x *.copy
		return
	fi
	7z x ${ENV}_DUMP_NASSUR_last_dump.zip
}
##########################################################################
function dropCreateDatabase()
{
	echo "Drop des bases"
	mysql -u $USER -p$PASSWORD -e "DROP DATABASE ${CLIENT}_fait;" &
	showWaiting $! "  Faits "
	mysql -u $USER -p$PASSWORD -e "DROP DATABASE ${CLIENT}_param;" &
	showWaiting $! "  Params "
	mysql -u $USER -p$PASSWORD -e "DROP DATABASE ${CLIENT}_static;" &
	showWaiting $! "  Statics "
	mysql -u $USER -p$PASSWORD -e "DROP DATABASE ${CLIENT}_camunda;" &
	showWaiting $! "  Camunda "

	echo "Création des bases"
	mysql -u $USER -p$PASSWORD -e "CREATE DATABASE ${CLIENT}_fait;"
	mysql -u $USER -p$PASSWORD -e "CREATE DATABASE ${CLIENT}_param;"
	mysql -u $USER -p$PASSWORD -e "CREATE DATABASE ${CLIENT}_static;"
	mysql -u $USER -p$PASSWORD -e "CREATE DATABASE ${CLIENT}_camunda;"
	echo " "
}
##########################################################################
function import()
{
	echo "Import des bases"
	mysql -u $USER -p$PASSWORD ${CLIENT}_static < $STATIC_FILE &
	showWaiting $! "  Statics "

	mysql -u $USER -p$PASSWORD ${CLIENT}_param < $PARAMS_FILE &
	showWaiting $! "  Params "

	mysql -u $USER -p$PASSWORD ${CLIENT}_fait < $FAITS_ROUTINES &
	showWaiting $! "  Faits routines (1/3) "

	mysql -u $USER -p$PASSWORD ${CLIENT}_fait < $FAITS &
	showWaiting $! "  Faits (2/3) "

	mysql -u $USER -p$PASSWORD ${CLIENT}_fait < $FAITS_VIEWS &
	showWaiting $! "  Faits views (3/3) "
}
##########################################################################
function anonymisation()
{
	if [[ -d "${ANONYMISATION_FOLDER}" ]]
    then
		echo "Anonymisation NOM, RAISON SOCIAL, EMAIL, TEL"
		echo Pseudonymisation tables des personnes
		mysql -u $USER -p$PASSWORD ${CLIENT}_fait --force --default_character_set utf8 < "${ANONYMISATION_FOLDER}/001_personnes.sql"
		echo Pseudonymisation tables adresses
		mysql -u $USER -p$PASSWORD ${CLIENT}_fait --force --default_character_set utf8 < "${ANONYMISATION_FOLDER}/002_adresses.sql"
		echo Pseudonymisation tables des véhicules
		mysql -u $USER -p$PASSWORD ${CLIENT}_fait --force --default_character_set utf8 < "${ANONYMISATION_FOLDER}/003_vehicules.sql"
	else
		echo ""
		echo "ATTENTION !!! LE DOSSIER D'ANONYMISATION" ${ANONYMISATION_FOLDER} "N'EXISTE PAS !!!"
		echo ""
    fi

	echo "Init mot de passe utilisateur"
	mysql -u $USER -p$PASSWORD -e "update ${CLIENT}_param.p_x_utilisateur set uti_mot_de_passe='744acd4d978d2d985296b3b0d389c79482502814' where uti_login<>'admin';"

	echo " "
	echo "Annulation des taches"
	mysql -u $USER -p$PASSWORD -e "update ${CLIENT}_fait.f_tache set tac_etat_code = 'ANN', tac_etat_libelle = 'Annulée' where tac_type_code in ('REL', 'EXE') and tac_etat_code in ('INIT','ENC');"

	echo " "
	echo "Annulation message"
	mysql -u $USER -p$PASSWORD -e "update ${CLIENT}_fait.f_dis_message set dim_distribution_etat = 'ANNULE' where dim_distribution_etat = 'EN_ATTENTE';"

	#echo " "
	#echo "Annulation cron"
	#mysql -u $USER -p$PASSWORD -e "update ${CLIENT}_param.p_x_batch set btc_cron = null";
	#echo " "

}
##########################################################################
function cleanFolder()
{
	echo " "
	echo "Suppression sql dezippes"
	rm -f *.sql
	rm -f *.copy
	if [[ -z $FILE ]]
    then
		rm -f ${ENV}_DUMP_NASSUR_last_dump.zip
	fi
	echo " "
	echo "Fin"
	exit 0
}
##########################################################################
function showWaiting()
{
    i=0
    sp="/-\|"
    msg=$2
    while [ -d /proc/$1 ]
    do
       i=$(( (i+1) %4 ))
      printf "\r${msg}${sp:$i:1}"
      sleep .1
    done
    echo " "
    echo " "
}
##########################################################################
function main()
{
    showLogo
    copyLastDump
	unzipLastDump
	dropCreateDatabase
    import
    anonymisation
    cleanFolder
}
##########################################################################
main
