# GLOBAL VARIABLES
VERSION=1.3
WORKING_FOLDER=/c/nassur/dumps/odi
SHARED_FOLDER=/c/nassur/dumps/partage
FOLDER=$(date +%Y)-$(date +%m)
FILE_SUFFIX=$(date +%Y)-$(date +%m)-$(date +%d)

##########################################################################
function usage()
{
    echo "Usage : "
    echo "  --cygwin                              Si le bash est celui Cygwin"
    echo "  -v|--version                          Affiche la version du script"
    echo "  -h|--help                             Documentation"
    echo "  -u=<USER>|--user=<USER>               Mysql user"
    echo "  -p=<PASSWORD>|--password=<PASSWORD>   Mysql password"
    echo "  -e=<ENV>|--env=<ENV>                  Environment : [REC, PREPROD, PROD]"
    echo "  -d=<DEST_FOLDER>|--dest=<DEST_FOLDER> Dossier destination dump"
    exit 0   
}
##########################################################################
function showLogo()
{
    echo "      _                                "
    echo "     | |                               " 
    echo "   __| |_   _ _ __ ___  _ __  _   _    "
    echo "  / _\` | | | | '_ \` _ \| '_ \| | | | "
    echo " | (_| | |_| | | | | | | |_) | |_| |   "
    echo "  \__,_|\__,_|_| |_| |_| .__/ \__, |   "
    echo "                       | |     __/ |   "
    echo "                       |_|    |___/    v${VERSION}"
    echo ""
}
##########################################################################
#### ARGUMENTS
for i in "$@"; do 
    case $i in 
        -v|--version)
            showLogo
            echo "Dumpy : v${VERSION}"
            exit 0
            shift
            ;;
        --cygwin)
            WORKING_FOLDER=/cygdrive/c/nassur/dumps/odi
            SHARED_FOLDER=/cygdrive/c/nassur/dumps/partage
            shift
            ;;
        -u=*|--user=*)
            USER_INPUT="${i#*=}"
            shift
            ;;
        -p=*|--password=*)
            PASSWORD_INPUT="${i#*=}"
            shift
            ;;
        -e=*|--env=*)
            ENV_INPUT="${i#*=}" 
            shift
            ;;
        -d=*|dest=*)
            DEST_FOLDER="${i#*=}" 
            shift
            ;;       
        -h|--help)
            usage
            ;;    
        *)
            echo "Argument inconnu"
            usage
            ;;
    esac
done   
##########################################################################
##### OVERRIDE DU FICHIER ENV PAR LES ARGS EN PARAMETRE
if [[ ! -z $USER_INPUT ]]
then    
    USER=$USER_INPUT
fi
if [[ ! -z $PASSWORD_INPUT ]]
then    
    PASSWORD=$PASSWORD_INPUT
fi    
if [[ ! -z $ENV_INPUT ]]
then    
    ENV=$ENV_INPUT
fi 
if [[ -z $USER ]] || [[ -z $PASSWORD ]] || [[ -z $ENV ]]
then
    echo "Certains arguments sont manquants : "
    echo "MYSQL_USER : "${USER}
    echo "MYSQL_PASSWORD : "${PASSWORD}
    echo "ENV : "${ENV}
    echo " "
    usage
    exit 1
fi
##########################################################################
function createDumpFolder()
{      
    if [[ ! -z "${DEST_FOLDER}" ]]
    then
        return
    fi
    echo "Création du répertoire" ${WORKING_FOLDER}/${FOLDER}
    mkdir -p ${WORKING_FOLDER}/${FOLDER}
}
##########################################################################
function dumpDatabase()
{
    mysqldump -u $USER -p$PASSWORD odi > odi.sql &
    showWaiting $! "Dump de la base odi "

    mysqldump -u $USER -p$PASSWORD odi o_flux_config > odi_flux_config.sql
}
##########################################################################
function compressAndMoveDump()
{
    7z a -t7z ${ENV}_DUMP_ODI_${FILE_SUFFIX}.zip odi.sql odi_flux_config.sql &
    showWaiting $! "Compression du dump "

    echo "Déplacement du dump"
    if [[ ! -z "${DEST_FOLDER}" ]]
    then
        mv -f ${ENV}_DUMP_ODI_${FILE_SUFFIX}.zip ${DEST_FOLDER}/${ENV}_DUMP_ODI_last_dump.zip
        return
    fi  
    if [[ ! -d "${SHARED_FOLDER}" ]]
    then
        mkdir -p ${SHARED_FOLDER}
    fi
    yes | cp -rf ${ENV}_DUMP_ODI_${FILE_SUFFIX}.zip ${SHARED_FOLDER}/${ENV}_DUMP_ODI_last_dump.zip
    mv -f ${ENV}_DUMP_ODI_${FILE_SUFFIX}.zip ${WORKING_FOLDER}/${FOLDER}
}
##########################################################################
function cleanFolder()
{
    echo "Suppression des fichiers temporaires"
    rm odi.sql odi_flux_config.sql
}
##########################################################################
function showWaiting()
{ 
    i=0
    sp="/-\|"
    msg=$2
    while [ -d /proc/$1 ]
    do
       i=$(( (i+1) %4 ))
      printf "\r${msg}${sp:$i:1}"
      sleep .1
    done
    echo " "
    echo " "
}
##########################################################################
function main()
{
    showLogo
    createDumpFolder
    dumpDatabase
    compressAndMoveDump
    cleanFolder
}
##########################################################################
main

echo "Fin"








