# GLOBAL VARIABLES
VERSION=1.3
SHARED_FOLDER=/c/nassur/dumps/partage

##########################################################################
function usage()
{
    echo "Usage : "
    echo "  --cygwin                            Si le bash est celui Cygwin"
    echo "  -v|--version                        Affiche la version du script"
    echo "  -f=<FILE>|--file=<FILE>             Fichier zip, défaut dans $SHARED_FOLDER"
    echo "  -h|--help                           Documentation"
    echo "  -u=<USER>|--user=<USER>             Mysql user"
    echo "  -p=<PASSWORD>|--password=<PASSWORD> Mysql password"
    echo "  -e=<ENV>|--env=<ENV>                Environment : [REC, PREPROD, PROD]"
    exit 0   
}
##########################################################################
function showLogo()
{
	echo "            _                      _       _  _  "
	echo "   __      | |    __ _     __     | |__   | || | "
	echo "  / _|     | |   / _\` |   / _|    | / /    \_, |   "
	echo "  \__|_   _|_|_  \__,_|   \__|_   |_\_\   _|__/  "
	echo "_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_| \"\"\"\"| "
	echo "\"\`-0-0-'\"\`-0-0-'\"\`-0-0-'\"\`-0-0-'\"\`-0-0-'\"\`-0-0-'  v${VERSION}"	
	echo ""
}
##########################################################################
rm -f *.copy
for i in "$@"; do
  	case $i in
	  	-v|--version)
            showLogo
            echo "Clacky : v${VERSION}"
            exit 0
            shift
            ;;
		-f=*|--file=*)
			FILE="${i#*=}"
			shift
			;;
		--cygwin)
			SHARED_FOLDER=/cygdrive/c/nassur/dumps/partage
			shift
            ;;
        -u=*|--user=*)
            USER_INPUT="${i#*=}"
            shift
            ;;
        -p=*|--password=*)
            PASSWORD_INPUT="${i#*=}"
            shift
            ;;
        -e=*|--env=*)
            ENV_INPUT="${i#*=}" 
            shift
            ;;
		-h|--help)
            usage
            ;;  
		*)
			echo "Argument inconnu"
            usage
			;;
  esac
done
if [[ ! -z $FILE ]] && [[ ! -f $FILE ]]
then
	echo "File not found ${FILE}"
	exit 1
fi
##########################################################################
##### OVERRIDE DU FICHIER ENV PAR LES ARGS EN PARAMETRE
if [[ ! -z $USER_INPUT ]]
then    
    USER=$USER_INPUT
fi
if [[ ! -z $PASSWORD_INPUT ]]
then    
    PASSWORD=$PASSWORD_INPUT
fi    
if [[ ! -z $ENV_INPUT ]]
then    
    ENV=$ENV_INPUT
fi 
if [[ -z $USER ]] || [[ -z $PASSWORD ]] || ( [[ -z $ENV ]] && [[ -z $FILE ]] )
then
    echo "Certains arguments sont manquants : "
    echo "MYSQL_USER : "${USER}
    echo "MYSQL_PASSWORD : "${PASSWORD}
    echo "ENV : "${ENV} " Remplir si FILE est vide"
    echo "FILE : "${FILE}
    echo " "
    usage
    exit 1
fi
##########################################################################
function copyLastDump()
{
	if [[ ! -z $FILE ]]
	then
		COPY=$(basename "$FILE")
		EXTENTION="${COPY##*.}"
		FILENAME="${COPY%.*}"
		cp $FILE ${FILENAME}.copy
		return
	fi
	echo "Copie du dernier dump de ${ENV}"
	rm -f ${ENV}_DUMP_ODI_last_dump.zip
	yes | cp -rf ${SHARED_FOLDER}/${ENV}_DUMP_ODI_last_dump.zip .
	echo " "
}
##########################################################################
function unzipLastDump()
{
	echo "Décompression du dump"
	echo " "
	if [[ ! -z $FILE ]]
	then
		7z x *.copy
		return
	fi
	7z x ${ENV}_DUMP_ODI_last_dump.zip
}
##########################################################################
function dropCreateDatabase()
{
	echo "Drop de la base"
	mysql -u $USER -p$PASSWORD -e "DROP DATABASE odi;"
	echo " "

	echo "Création de la base"
	mysql -u $USER -p$PASSWORD -e "CREATE DATABASE odi;"
	echo " "

}
##########################################################################
function import()
{
	mysql -u $USER -p$PASSWORD odi < odi.sql &
	showWaiting $! "Import odi "
} 
##########################################################################
function cleanFolder()
{
	echo "Suppression des fichiers temporaires"
	rm -f *.sql
	rm -f *.copy
	rm -f ${ENV}_DUMP_ODI_last_dump.zip
	echo "Fin"
	exit 0
}
##########################################################################
function showWaiting()
{ 
    i=0
    sp="/-\|"
    msg=$2
    while [ -d /proc/$1 ]
    do
       i=$(( (i+1) %4 ))
      printf "\r${msg}${sp:$i:1}"
      sleep .1
    done
    echo " "
    echo " "
}
##########################################################################
function main()
{
    showLogo
    copyLastDump
	unzipLastDump
	dropCreateDatabase
    import
    cleanFolder
}
##########################################################################
main

echo "Fin"