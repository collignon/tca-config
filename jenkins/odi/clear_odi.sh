VERSION=1.3
FOLDER=/c/nassur/odi

##########################################################################
function usage()
{
    echo "Usage : "
    echo "  --cygwin                              Si le bash est celui Cygwin"
    echo "  -v|--version                          Affiche la version du script"
    echo "  -h|--help                             Documentation"
    echo "  -u=<USER>|--user=<USER>               Mysql user"
    echo "  -p=<PASSWORD>|--password=<PASSWORD>   Mysql password"
    exit 0   
}
##########################################################################
function showLogo()
{
    echo "  _                                       "
    echo " | |                                      " 
    echo " | |__  _ __ ___   ___  _ __ ___  _   _   "
    echo " | '_ \| '__/ _ \ / _ \| '_ \` _ \| | | | "
    echo " | |_) | | | (_) | (_) | | | | | | |_| |  "
    echo " |_.__/|_|  \___/ \___/|_| |_| |_|\__, |  "
    echo "                                   __/ |  "
    echo "                                  |___/   v${VERSION}"
    echo ""
}
##########################################################################
#### ARGUMENTS
for i in "$@"; do 
    case $i in 
        -v|--version)
            showLogo
            echo "Broomy : v${VERSION}"
            exit 0
            shift
            ;;
        --cygwin)
            FOLDER=/cygdrive/c/nassur/odi
            shift
            ;;
        -u=*|--user=*)
            USER_INPUT="${i#*=}"
            shift
            ;;
        -p=*|--password=*)
            PASSWORD_INPUT="${i#*=}"
            shift
            ;;    
        -h|--help)
            usage
            ;;    
        *)
            echo "Argument inconnu"
            usage
            ;;
    esac
done       
##########################################################################
##### OVERRIDE DU FICHIER ENV PAR LES ARGS EN PARAMETRE
if [[ ! -z $USER_INPUT ]]
then    
    USER=$USER_INPUT
fi
if [[ ! -z $PASSWORD_INPUT ]]
then    
    PASSWORD=$PASSWORD_INPUT
fi
if [[ -z $USER ]] || [[ -z $PASSWORD ]]
then
    echo "Environment variables are missing : "
    echo "MYSQL_USER : "${USER}
    echo "MYSQL_PASSWORD : "${PASSWORD}
    echo " "
    usage    
    exit 1
fi
##########################################################################
function cleanOdi()
{
    echo "################## NETTOYAGE DE LA BASE DE FAIT ODI"
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_compta_paiement;'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_cotisation;'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_contrat_mouvement_risque;'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_sinistre_mouvement;'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM x_sinistre_reglement'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_sinistre'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_contrat'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_client'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_flux_retour'
    mysql -u $USER -p$PASSWORD -e 'USE odi; DELETE FROM o_flux'
    echo "FIN NETTOYAGE"
}
##########################################################################
function deleteFolder()
{
    echo "################## SUPPRESSION DES REPERTOIRES ${FOLDER}/archives ${FOLDER}/fluxSaved" 
    cd ${FOLDER}
    rm -Rf archives fluxSaved
}        
##########################################################################
function main()
{
    showLogo
    cleanOdi
    deleteFolder
}
##########################################################################
main

echo "################## FIN"