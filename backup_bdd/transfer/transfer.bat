@echo off
mode con cols=150 lines=50
Title Transfert base serveur
echo -----------------------------------------------------------------------------------
echo Lancement de la procedure de TRANSFERT des bases de donnees
echo -----------------------------------------------------------------------------------

for %%d in (%~dp0..) do set path_parent=%%~fd
FOR /F "tokens=1,2 delims==" %%G IN (%path_parent%\config\variables.properties) DO (set %%G=%%H)

echo.
echo Vous allez lancer la procedure de transfert de dump du serveur %source% vers le votre

echo.
echo DEBUT du TRAITEMENT

echo.
echo TRANSFERT DES DUMPS
copy /Y %transfer.source% %transfer.cible%

echo.
echo FIN du TRAITEMENT