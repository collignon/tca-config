@echo off
mode con cols=150 lines=50
Title Import base serveur

for %%d in (%~dp0..) do set path_parent=%%~fd
FOR /F "tokens=1,2 delims==" %%G IN (%path_parent%\config\variables.properties) DO (set %%G=%%H)

echo -----------------------------------------------------------------------------------
echo Lancement de la procedure d IMPORT des bases de donnees de %import.export_environnement%
echo -----------------------------------------------------------------------------------

echo.
echo Vous allez lancer la procedure d import des bases de donnees

set dump_folder=%path_parent%\dumps_to_import

set mysql_user=%path_parent%\config\mysql_user.cnf

set anonymisation_folder=%path_parent%\anonymise-pseudonymise\scripts_pseudonymise


echo.
echo 1. REINIT BASES
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "drop database if exists %import.prefixe_name_bdd%_static;"
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "create database %import.prefixe_name_bdd%_static;"
 
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "drop database if exists %import.prefixe_name_bdd%_param;"
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "create database %import.prefixe_name_bdd%_param;"
 
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "drop database if exists %import.prefixe_name_bdd%_fait;"
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "create database %import.prefixe_name_bdd%_fait;"

echo.
echo 2. IMPORT static
mysql --defaults-extra-file=%mysql_user% -h %import.host% %import.prefixe_name_bdd%_static < %dump_folder%\dump_static.sql

echo.
echo 3. IMPORT param
mysql --defaults-extra-file=%mysql_user% -h %import.host% %import.prefixe_name_bdd%_param < %dump_folder%\dump_param.sql

echo.
echo 4. IMPORT routines fait
mysql --defaults-extra-file=%mysql_user% -h %import.host% %import.prefixe_name_bdd%_fait < %dump_folder%\dump_fait_routine.sql

echo.
echo 5. IMPORT fait
mysql --defaults-extra-file=%mysql_user% -h %import.host% %import.prefixe_name_bdd%_fait < %dump_folder%\dump_fait.sql

echo.
echo 6. ANONYMISATION des donnees
echo Pseudonymisation tables des personnes
mysql --defaults-extra-file=%mysql_user% %import.prefixe_name_bdd%_fait --force --default_character_set utf8 < "%anonymisation_folder%/001_personnes.sql"
echo Pseudonymisation tables adresses
mysql --defaults-extra-file=%mysql_user% %import.prefixe_name_bdd%_fait --force --default_character_set utf8 < "%anonymisation_folder%/002_adresses.sql"
echo Pseudonymisation tables des véhicules
mysql --defaults-extra-file=%mysql_user% %import.prefixe_name_bdd%_fait --force --default_character_set utf8 < "%anonymisation_folder%/003_vehicules.sql"

echo.
echo 7. ANNULER taches
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "update %import.prefixe_name_bdd%_fait.f_tache set tac_etat_code = 'ECHEC', tac_etat_libelle = 'Echec' where tac_type_code in ('REL', 'EXE') and tac_etat_code = 'ENC';"
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "update %import.prefixe_name_bdd%_fait.f_dis_message set dim_distribution_etat = 'ECHEC' where dim_distribution_etat = 'EN_ATTENTE';"


echo.
echo 9. MISE A NULL DES CRON
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "update %import.prefixe_name_bdd%_param.p_x_batch set btc_cron = null;"

echo.
echo 10. Mot de passe utilisateurs
mysql --defaults-extra-file=%mysql_user% -h %import.host% -e "update %import.prefixe_name_bdd%_param.p_x_utilisateur set uti_mot_de_passe='744acd4d978d2d985296b3b0d389c79482502814' where uti_login<>'admin';"

echo.
echo FIN du TRAITEMENT

pause;