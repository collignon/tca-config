@echo off
set dateCode=%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%
set dateCode=%dateCode: =0%
set timeCode=%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%
set timeCode=%timeCode: =0%
for %%d in (%~dp0..) do set path_parent=%%~fd

echo lancer le traitement? (O/N)
set /p val=enter a value: 
if %val% == n set val=N
if %val% == N exit

%~dp0\import_bdd.bat 2>&1 | %path_parent%\bin\tee %path_parent%\log\export\%dateCode%_%timeCode%logfile.txt

pause