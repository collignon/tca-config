Param(
[string]$logfile,
[string]$errorfile,
[string]$environnement
)

$Email       = "cmam-team@okayo.fr"
$Internal    = "l.jacquier@cmam.fr"
$Subject     = "CMAM - Backup " + $environnement


$Msg = @{
    to          = $Email
    cc          = $Internal
    from        = "noreply@cmam.fr"
    Body        = [IO.File]::ReadAllText($logfile) + [IO.File]::ReadAllText($errorfile)
    subject     = "$Subject"
    smtpserver  = "relais-smtp.activeweb.fr"
    BodyAsHtml  = $False
	Attachments = $logfile,$errorfile
	#Attachments = $logfile,$errorfile
}

Send-MailMessage @Msg