set dateCode=%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%
set dateCode=%dateCode: =0%
set timeCode=%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%
set timeCode=%timeCode: =0%
for %%d in (%~dp0..) do set path_parent=%%~fd

set root_log_name=%path_parent%\log\export\%dateCode%_%timeCode%
set log_file=%root_log_name%logfile.txt
set error_file=%root_log_name%errorlog.txt

call %~dp0\export_bdd.bat > "%log_file%" 2>"%error_file%"
call powershell -File .\send_mail_log.ps1 -logfile %log_file% -errorfile %error_file% -environnement %export.environnement%

exit