@echo off
mode con cols=150 lines=50
Title Traitement backup base
echo -----------------------------------------------------------------------------------
echo Lancement de la procedure d EXPORT des bases de donnees de REC
echo -----------------------------------------------------------------------------------
echo.
echo Pensez a verifier que les properties correspondent a l'action en cours
echo.

echo.
echo DEBUT du TRAITEMENT

for %%d in (%~dp0..) do set path_parent=%%~fd
FOR /F "tokens=1,2 delims==" %%G IN (%path_parent%\config\variables.properties) DO (set %%G=%%H)

set dateCode=%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%
set dateCode=%dateCode: =0%
set timeCode=%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%
set timeCode=%timeCode: =0%
set name_save_folder=%dateCode%_%timeCode%_dump
set path_work_folder=%path_parent%\dumps_exported\%export.environnement%_%name_save_folder%

set mysql_user=%path_parent%\config\mysql_user.cnf

mkdir %path_work_folder%

echo.
echo 1. DUMP des statics
mysqldump --defaults-extra-file=%mysql_user% -h %export.host% %export.prefixe_name_bdd%_static > %path_work_folder%\dump_static.sql

echo.
echo 2. DUMP des params
mysqldump --defaults-extra-file=%mysql_user% -h %export.host% %export.prefixe_name_bdd%_param > %path_work_folder%\dump_param.sql

echo.
echo 3. DUMP des routines fait
mysqldump --defaults-extra-file=%mysql_user% -h %export.host% --routines --no-create-info --no-data --no-create-db --skip-opt %export.prefixe_name_bdd%_fait | %path_parent%\bin\sed -e "s/DEFINER=[^ ]* / /" > %path_work_folder%\dump_fait_routines.sql

echo.
echo 4. DUMP des faits
mysqldump --defaults-extra-file=%mysql_user% -h %export.host% %export.prefixe_name_bdd%_fait | %path_parent%\bin\sed -e "s/DEFINER=[^ ]* / /" > %path_work_folder%\dump_fait.sql

echo.
echo 5. ZIP
%path_parent%\bin\zip.exe -rjm %path_work_folder%.zip %path_work_folder%
copy /Y %path_work_folder%.zip %path_parent%\dumps_exported\%export.environnement%_last_dumps.zip

echo.
echo FIN du TRAITEMENT
pause