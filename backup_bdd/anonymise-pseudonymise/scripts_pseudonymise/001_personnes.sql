﻿select @max_noms := count(*) from cmam_data.a_nom;
select @max_prenoms := count(*) from cmam_data.a_prenom;


update f_personne_physique
SET    pph_nom = (select nom_nom from cmam_data.a_nom order by rand() limit 1)
     , pph_prenom = (select pre_prenom from cmam_data.a_prenom order by rand() limit 1);

update f_personne_physique JOIN f_personne ON prs_id = pph_ptrpersonneident 
   set prs_nom_appel = concat(pph_prenom, ' ', pph_nom);

UPDATE f_personne_morale JOIN f_personne ON prs_id = pmo_ptrpersonneident
SET    pmo_raison_sociale = (select concat('STE ', nom_nom) from cmam_data.a_nom order by rand() limit 1),
       pmo_nom_commercial = (select concat('STE ', nom_nom) from cmam_data.a_nom order by rand() limit 1)
WHERE  pmo_estentitegestion = 0
AND NOT EXISTS(select str_id from f_structure where f_structure.str_ptridpersonne = pmo_ptrpersonneident);

UPDATE f_personne_morale JOIN f_personne ON prs_id = pmo_ptrpersonneident
SET    prs_nom_appel = pmo_raison_sociale
WHERE  pmo_estentitegestion = 0
AND NOT EXISTS(select str_id from f_structure where f_structure.str_ptridpersonne = pmo_ptrpersonneident);
