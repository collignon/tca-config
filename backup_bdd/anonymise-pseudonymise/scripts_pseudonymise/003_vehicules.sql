﻿update f_prod_orv_vehicule  
join f_prod_objet_risque_version ON orv_id = veh_ptr_objet_risq_version_id
join f_prod_con_version ON cov_id = orv_ptr_contrat_version_id
join f_prod_contrat ON con_id = cov_ptr_contrat_id
set  veh_immatriculation = concat(
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(con_id)*4294967296))*36+1, 1),
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  '-',
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  '-',
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a)*36+1, 1)
)
where veh_immatriculation is not null;


update f_sin_cov_vehicule 
join f_sin_objet_risque ON obr_id = svh_ptr_objetrisque_id
join f_sin_con_version ON cov_id = obr_ptr_version_id
set svh_immatriculation = concat(
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(cov_identifiant_contrat)*4294967296))*36+1, 1),
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  '-',
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  '-',
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a:=round(rand(@a)*4294967296))*36+1, 1),
  substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', rand(@a)*36+1, 1)
) where svh_immatriculation is not null;

