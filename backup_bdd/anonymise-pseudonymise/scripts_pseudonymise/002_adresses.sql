﻿set @nom_domaine := 'okayo.fr';

UPDATE f_adresse_mail join f_personne ON prs_id = adm_ptr_personne_id
                 left join f_personne_morale ON prs_id = pmo_ptrpersonneident
SET    adm_mail = concat(lower(replace(prs_nom_appel,' ','.')), '@', @nom_domaine)
where coalesce(pmo_estentitegestion,0) = 0
AND NOT EXISTS(select str_id from f_structure where f_structure.str_ptridpersonne = pmo_ptrpersonneident);

UPDATE f_dis_message 
set     dim_adresse_envoi     = IF(dim_adresse_envoi is null, null, concat(substring(1000000 * RAND(dim_id), 1, 6), '@okayo.fr'))
      , dim_from              = IF(dim_from is null, null, concat(substring(1000000 * RAND(dim_id), 1, 6), '@okayo.fr'))
      , dim_adresse_envoi_cc  = IF(dim_adresse_envoi_cc is null, null, concat(substring(1000000 * RAND(dim_id), 1, 6), '@okayo.fr'))
      , dim_adresse_envoi_cci = IF(dim_adresse_envoi_cci is null, null, concat(substring(1000000 * RAND(dim_id), 1, 6), '@okayo.fr'));


UPDATE f_adresse_telephonique join f_personne ON prs_id = adt_ptr_personne_id
                         left join f_personne_morale ON prs_id = pmo_ptrpersonneident
SET    adt_telephone = concat('0', substring(10000000000 * RAND(adt_id), 1, 9))
where coalesce(pmo_estentitegestion,0) = 0
AND NOT EXISTS(select str_id from f_structure where f_structure.str_ptridpersonne = pmo_ptrpersonneident);

UPDATE f_adresse_postale  join f_personne ON prs_id = adp_ptr_personne_id
                     left join f_personne_morale ON prs_id = pmo_ptrpersonneident
SET    adp_ligne_1 = if(adp_ligne_1 IS NULL, NULL, md5(adp_id)), adp_ligne_2 = if(adp_ligne_2 IS NULL, NULL, md5(adp_id * 2)),
       adp_ligne_3 = if(adp_ligne_3 IS NULL, NULL, md5(adp_id * 3))
where coalesce(pmo_estentitegestion,0) = 0
AND NOT EXISTS(select str_id from f_structure where f_structure.str_ptridpersonne = pmo_ptrpersonneident);

UPDATE f_adresse_bancaire
SET    adb_compte_titulaire = substring(1000000 * RAND(adb_ptr_personne_id), 1, 6);

UPDATE f_adresse_bancaire JOIN f_personne ON prs_id = adb_ptr_personne_id
SET    adb_compte_titulaire = prs_nom_appel;

update f_adresse_bancaire set adb_iban_code = 'FR7600000000000000000000097' ;

update f_pai_paiement set pai_sepa_titulaire = 'XXX', pai_sepa_iban = 'FR7600000000000000000000097' where pai_sepa_iban is not null;
update f_pai_paiement set pai_tiers_tireur = 'X' where pai_tiers_tireur is not null;
