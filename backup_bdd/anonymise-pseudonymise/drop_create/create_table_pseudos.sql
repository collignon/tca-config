﻿drop table a_prenom;
drop table a_nom;

CREATE TABLE a_prenom (
  pre_id int(11) NOT NULL AUTO_INCREMENT,
  pre_prenom varchar(255) NOT NULL,
  pre_services text,
  PRIMARY KEY (pre_id)
) ;


CREATE TABLE a_nom (
  nom_id int(11) NOT NULL AUTO_INCREMENT,
  nom_nom varchar(255) NOT NULL,
  nom_services text,
  PRIMARY KEY (nom_id)
) ;