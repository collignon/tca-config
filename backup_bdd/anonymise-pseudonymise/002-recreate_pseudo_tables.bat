@echo off
cd /d %~dp0

:execute_fait
echo %DATE%
echo %TIME%

set bdd_cible=cmam_data

@echo off

echo create table pseudos
call mysql -u root -proot %bdd_cible% --force --default_character_set utf8 < "drop_create/create_table_pseudos.sql"

echo load tables pseudos
call mysql -u root -proot %bdd_cible% --force --default_character_set utf8 < "generated\001-pseudo_tables.sql"



pause